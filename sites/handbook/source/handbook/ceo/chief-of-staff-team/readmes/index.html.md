---
layout: markdown_page
title: "Chief of Staff Team to the CEO READMEs"
---
## Chief of Staff Team to the CEO READMEs

- [Omar's README (Director of Strategy and Operations)](https://gitlab.com/ofernandez2/readme)
- [Stella's README (Chief of Staff to the CEO)](/handbook/ceo/chief-of-staff-team/readmes/streas/)
