---
layout: handbook-page-toc
title: "Market Strategy and Insights"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Market Strategy and Insights (MSI) at GitLab

**Who We Are**
- A Team of Experienced Customer Reference, Market Insights, and Industry Analyst Relations Professionals

**What We Do**
- Gather & Build - Information & Relationships
- Distill - Impactful Insights
- Activate - Turning Insights Into Action

**How We Do It**
- [**Customer Reference Program**](/handbook/marketing/strategic-marketing/customer-reference-program/)
  - [Customer Advisory Board](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/CAB/)
  - [Customer Case Studies](https://about.gitlab.com/customers/)
  - [Peer Review Site Management](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/peer-reviews/) (Gartner Peer Insights, G2, Trust Radius, etc.)
  - [Customer Insight Portraits](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/)
- [**Industry Analyst Relations (IAR)**](/handbook/marketing/strategic-marketing/analyst-relations/)
  - Interactions ([Briefings](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/#how-we-conduct-industry-analyst-briefings), [Inquiry](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/#how-we-conduct-industry-analyst-inquiries), Strategic Advisory)
  - [Publications](https://about.gitlab.com/analysts/) (Comparative Research, Market Landscape, Best Practices, Thought Leadership)
  - View the [Analyst Relations Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/940099?&label_name[]=Analyst%20Relations) and [AR-Cats Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/940116?&label_name[]=Analyst%20Relations) to see what's currently in progress.

**Why We Do It**
 - Simplify Selling
 - Help Build Better Products
 - Improve the Customer Experience and Value Realized

### Which Market Strategy and Insights team member should I contact?

  - Listed below are areas of responsibility within the Market Research and Customer Insight team:

    - [Ryan](/company/team/#ryanragozzine), Analyst Relations Manager
    - [Fiona](/company/team/#fokeeffe), Reference Program Manager; Regional Lead: EMEA and APAC; Program Lead: Customer Advisory Board, Customer Speaking Requests, Industry Analyst Relations Requests
    - [Jen](/company/team/#jlparker), Reference Program Manager; Regional Lead: Americas; Program Lead: Customer Insights/Case Studies, Peer Reviews, ReferenceEdge
    - [Traci](/company/team/#tracirobinson), Manager, Market Insights
    - [Laura](/company/team/#lclymer), Director, Market Strategy and Insights

#### Market Strategy and Insights Metrics 
- Target: Minimum 2 case studies published per month
  - Status: Met target consistently for Q2-Q4 FY21

