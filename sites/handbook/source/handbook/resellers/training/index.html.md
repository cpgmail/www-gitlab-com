---
layout: handbook-page-toc
title: "Channel Partner Training, Certifications, and Enablement"
description: "To support and scale GitLab’s continued growth and success, the  Enablement Team is developing a learning program that includes functional, soft skills, and technical training for channel and alliances partners"
---

## Partner Training, Certifications, and Enablement  
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---

## Training, Certifications and Enablement

GitLab has created a robust training and certification program to enable partners in our ecosystem to sell, market, service and support GitLab prospects and customers.  Training materials and learning paths have been developed to support different partner roles:


*   Sales Professionals
*   Technical Sales Professionals
*   Technical and Consulting Services Professionals 

**Why get certified?**

<img src="/images/partnerenablement/doc-pencil.png" width="150" alt="" title="GitLab Certifications">    <img src="/images/partnerenablement/illustration_announcement.png" width="175" alt="" title="GitLab Certifications">

**Validate your skills, knowledge and receive recognition for your individual mastery in GitLab!**
**Help your company meet program requirements and capture the GitLab product sales and services opportunity!**

Training courses, certification exams and other enablement materials can be accessed through the [GitlLab Partner Portal](partners.gitlab.com).

### Training and Certifications for Sales Professionals

#### GitLab Sales Core
This is the basic certification for sales and the prerequisite for pre-sales technical professionals. The curriculum provides an overview of the market GitLab services, customer personas and needs, GitLab solutions and positioning of GitLab. 

By completing the GitLab Sales Core and scoring above 80% on the tests, you’ll earn a GitLab Verified Core Sales completion badge and contribute to the Sales certification program requirements for your company. To earn a full GitLab Sales certification, you need to complete both the GitLab Sales Core plus at least two Advanced Use Cases courses.  Estimated completion time: 3.5 hours

<details>
<summary markdown='span'>
  GitLab Sales Core
</summary>

<b>Best Practices Selling GitLab:</b>

<ul>
<li>Identifying Customer Pain</li>
<li>The Economic Buyer</li>
<li>Decision Criteria and Process</li>
<li>Competition</li>
</ul>

<b>DevOps Technology Landscape:</b>

<ul>
<li>The Software Development LifeCycle</li>
<li>Reduce Cycle Time</li>
<li>Increase Operational Efficiencies</li>
<li>A Seismic Shift in Application Security</li>
<li>Manage Your Toolchain Before it manages you</li>
<li>QUIZ:  DevOps Technology Landscape</li>
</ul>

<b>Welcome to GitLab:</b>

<ul>
<li>Introduction to GitLab</li>
<li>GitLab Values </li>
<li>QUIZ: Welcome To GitLab</li>
</ul>

<b>GitLab Portfolio:</b>

<ul>
<li>GitLab Portfolia</li>
<li>Tiers and Features</li>
<li>QUIZ:  GitLab Portfolio</li>
</ul>

<b>GitLab Customers and Personas:</b>

<ul>
<li>GitLab CUstomers</li>
<li>Customer Success Stories & Proof Points</li>
</ul>

</details>

<img src="/images/partnerenablement/gitlab-verified-sales-core-personal.png" width="150" alt="" title="GitLab Verified Sales Core Partner">


### Training and Certifications for Technical Sales Professionals (Solution Architects / Sales Engineers)

#### GitLab Solution Architect Core
This is the basic certification for pre-sales technical professionals and provides a deeper understanding of demonstrating, deploying, integrating and optimizing GitLab solutions.  This certification is a mix of online learning and hands-on labs. GitLab Sales Core is the prerequisite for GitLab Solution Architect Core.  GitLab Solution Architect Core meets the program requirement for Pre-sales technical certification, and individuals will recieve a GitLab Core Solution Architecture completion badge. NOTE: The hands-on lab components for this certification are not yet available for partners. Partners will be notified once it is available and will be able to earn a certification at that time. Estimated course completion time: 5 hours

<details>
  <summary markdown='span'>
  GitLab Solution Architect Core
</summary>

<b>GitLab Integrations:</b>

<ul>
<li>GitLab Integrations Overview</li>
<li>Jira Integration</li>
<li>Jenkins Integration Overview</li>
<li>Migrating from Jenkins to GitLab</li>
<li>GitLab Workflow with Jira issues and Jenkins Pipelines</li>
<li>Github Integration</li>
<li>GitLab as OAuth2 Authentication Service Provider</li>
<li>QUIZ: Integrations</li>
</ul>

<b>Technical Deep Dive:</b>

<ul>
<li>Auto DevOps</li>
<li>GitLab API</li>
<li>GitLab for Agile</li>
<li>GitLab Runners</li>
<li>GitLab High Availability (HA) and GitLab GEO</li>
<li>QUIZ: Technical Deep Dive</li>
</ul>

</details>

<img src="/images/partnerenablement/gitlab-verified-sales-architect-personal.png" width="150" alt="" title="GitLab Verified Solution Architect Core Partner">


### Training and Certifications for Technical and Consulting Services Professionals

**Training and Certifications**

GitLab offers technical certifications that are designed to ensure Partners have the ability to apply GitLab core competencies in the field and within their own practices.  To earn a certification, candidates must first complete all relevant course material and then pass the assessment test with a score of 80% or greater. 



**GitLab Professional Services Engineer (PSE) Certification**
[Details Here](https://about.gitlab.com/services/pse-certifications/pse-specialist/)

<img src="/images/channel-service-program/gitlab-certified-professional-services-badge.png" width="150" alt="" title="GitLab Certified Professional Services Engineer">

Earning Criteria: To earn this certification, candidates must complete the learning journey and all associated assessments in the pathways. Candidates can start their training from the [GitLab Partner Portal](https://partners.gitlab.com/prm/English/c/Training)


<p>
</p>

#### [GitLab Certified Trainer](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-certified-trainer-process/#gitlab-certified-trainer---cadidate-process-for-partners)



#### Training and Certifications

GitLab Partner Sales Professional Certification 

<img src="/images/partnerenablement/gitlab-certified-sales-professional-personal.png" width="150" alt="" title="GitLab Certified Sales Professional">

Individuals who have completed this certification are able to articulate GitLab solutions, ideal customer personas and needs, industry relevant insights, customer environments where opportunities for GitLab are most relevant, along with GitLab Advanced Use Case positioning.

Earning Criteria: Completion of the GitLab Sales Core foundational training plus a minimum of 2 (two) expanded Use Case trainings and score above 80% on the knowledge assessments. Estimated completion time: 45 minutes per Advanced Use Case course 


GitLab Partner Solutions Architect (SA) Certification **Q1 FY22**

<img src="/images/partnerenablement/gitlab-certified-solutions-architect-personal.png" width="150" alt="" title="GitLab Certified Solution Architect">



#### Certification Award Process

To sign up for a course, please visit the [GitLab Partner Portal](https://partners.gitlab.com/prm/English/c/Training). Upon successful completion of each learning path, individuals will be awarded a Badge of Completion and / or a Certificate. Badges and certifications will be delivered electronically via email, and will be granted no later than 10 business days after completion. 




### Additional Enablement Resources

*   Webcasts - GitLab hosts a monthly webinar the first Thursday of each month,  providing deep dive learning on key GitLab topics.  You can access Partner Webcast Archives in the Asset Library on the [GitLab Partner Portal. ](https://partners.gitlab.com/English/)
*   Technical - Pre-Sales, Services Engineers and Solution Architects can join the Channel SA’s for a monthly Tech Chat. Use this** **[Partner Tech Chat](https://gitlab.com/gitlab-com/partners/tech-chats)** **board to submit future topics and questions. 
*   GitLab Partners’ Marketing team members are invited to join our monthly Partner Marketing Webcasts. Learn about the latest Partner marketing campaigns, resources and more. 
*   The [GitLab Partner Portal ](https://partners.gitlab.com/English/)provides Partners with easy access to additional sales resources, webcast replays, competitive analysis, event calendar, Marketing campaigns, support and more. 
*   Newsletter - sign up [here ](https://gitlab.us19.list-manage.com/subscribe?u=5a5f55e4e0f03037d96416766&id=2321e18463)
*   [GitLab Virtual Commit ](https://about.gitlab.com/events/commit/) - Commit 2020 was a 24-hour virtual experience filled with practical DevOps strategies shared by leaders in development, operations, and security. You’ll discover innovative solutions to help you solve your industry's greatest challenges, including enhancing culture and shortening release times.
*   [GitLab Handbook Resources ](/handbook/resellers/) Start your handbook search on the Channel Partner Handbook Page 
